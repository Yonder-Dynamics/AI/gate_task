import rospy
import actionlib
import utils
import math
import globalvars
from geometry_msgs.msg import PoseStamped, PoseArray, Pose
from move_base_msgs.msg import MoveBaseGoal, MoveBaseAction, MoveBaseActionFeedback
from actionlib_msgs.msg import GoalID

SPINNING = "gate_spinner.SPINNING"
FOUND = "gate_spinner.FOUND"
FAILURE = "gate_spinner.FAILURE"
TIMEOUT_RECIEVING_POSE = "gate_spinner.TIMEOUT_RECIEVING_POSE"
TIMEOUT_RECIEVING_GATE = "gate_spinner.TIMEOUT_RECIEVING_GATE"
LOST_GATE = "gate_spinner.LOST_GATE"

MAX_WAIT = 1*60 # seconds
RATE = 10 # hz
ANGULAR_RES = 30 # degrees

def gate_spinner(info):
    r = rospy.Rate(RATE);

    # Retrieve necessary info
    gate_poses = utils.sub_request(globalvars.AR_POSE_TOPIC,
                                   PoseArray, max_wait=60)
    rover_pose = utils.sub_request(globalvars.ROVER_POSE_TOPIC,
                                   PoseStamped, max_wait=1*60)

    if rover_pose is None:
        return TIMEOUT_RECIEVING_POSE, None
    if gate_poses is None:
        return TIMEOUT_RECIEVING_GATE, None
    if len(gate_poses.poses) == 0:
      return LOST_GATE, None

    gate_pose = gate_poses.poses[0] # Use first pose

    def received_feedback(msg):
        received_feedback.feedback = msg
    received_feedback.feedback = None

    def received_gate(msg):
        received_gate.gate_rel_pos = msg
    received_gate.gate_rel_pos = None


    # Init ROS
    traverse_action = actionlib.SimpleActionClient(
        globalvars.MOVE_ACTION, MoveBaseAction)
    rospy.Subscriber(globalvars.AR_POSE_TOPIC, PoseArray, queue_size=10,
                     callback=received_gate)
    traverse_action.wait_for_server()

    # Construct path
    # Divide radius by two so rot point lies in between gate and rover
    radius = math.sqrt(
            (gate_pose.position.x-rover_pose.pose.position.y)**2+
            (gate_pose.position.y-rover_pose.pose.position.y)**2)
    #  radius = 1.5
    # Find circle center
    circle_x = gate_pose.position.x
    circle_y = gate_pose.position.y

    # Find starting angle on circle based on current rover position
    start_angle = math.atan2(
        rover_pose.pose.position.y - circle_y,
        rover_pose.pose.position.x - circle_x)

    # Generate circle
    for t in range(360//ANGULAR_RES):
        # Check callbacks

        p = Pose()
        angle = t/180*math.pi*ANGULAR_RES + start_angle
        p.position.x = circle_x + radius*(math.cos(angle))
        p.position.y = circle_y + radius*(math.sin(angle))

        goal = MoveBaseGoal()
        goal.target_pose.pose = p
        traverse_action.send_goal(goal)
        res = traverse_action.wait_for_result()
        if traverse_action.get_state() == utils.SUCCEEDED:
            continue
        else:
            return FAILURE, None

    # Finished path, not found
    return FOUND, None

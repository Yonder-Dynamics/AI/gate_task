import rospy
import actionlib
import utils
import math
import time
import globalvars
from geometry_msgs.msg import PoseStamped, PoseArray, Pose
from move_base_msgs.msg import MoveBaseActionGoal, MoveBaseAction, MoveBaseActionFeedback, MoveBaseGoal

FINISHED = "go_to_gate.FINISHED"
CONTINUING = "go_to_gate.CONTINUING"
FAILURE = "go_to_gate.FAILURE"
TIMEOUT_RECIEVING_POSE = "go_to_gate.TIMEOUT_RECIEVING_POSE"
TIMEOUT_RECIEVING_GATE = "go_to_gate.TIMEOUT_RECIEVING_GATE"
LOST_GATE = "go_to_gate.LOST_GATE"

MAX_WAIT = 1*10 # seconds
RATE = 10 # hz
ANGULAR_RES = 5 # degrees

def go_to_gate(info):
    r = rospy.Rate(RATE);
    #  gate_poses = info

    # Retrieve necessary info
    gate_poses = utils.sub_request(globalvars.AR_POSE_TOPIC,
                                   PoseArray, max_wait=10)
    rover_pose = utils.sub_request(globalvars.ROVER_POSE_TOPIC,
                                   PoseStamped, max_wait=1*10)

    if rover_pose is None:
        return TIMEOUT_RECIEVING_POSE, None
    if gate_poses is None:
        return TIMEOUT_RECIEVING_GATE, None
    if len(gate_poses.poses) == 0:
      return LOST_GATE, None

    gate_pose = gate_poses.poses[0] # Use first pose
    gate_rel_pose = gate_poses.poses[0] # Use first pose
    # Make pose relative
    gate_rel_pose.position.x -= rover_pose.pose.position.x
    gate_rel_pose.position.y -= rover_pose.pose.position.y

    def received_gate(msg):
        received_gate.gate_rel_pos = msg
    received_gate.gate_rel_pos = None

    def received_feedback(msg):
        received_feedback.feedback = msg
    received_feedback.feedback = None

    # Init ROS
    traverse_action = actionlib.SimpleActionClient(
        globalvars.MOVE_ACTION, MoveBaseAction)
    rospy.Subscriber(globalvars.AR_POSE_TOPIC, PoseArray, queue_size=10,
                     callback=received_gate)
    traverse_action.wait_for_server()


    dist = (gate_rel_pose.position.x ** 2 + gate_rel_pose.position.y ** 2) ** .5
    angle = math.atan2(gate_rel_pose.position.y, gate_rel_pose.position.x)


    our_point_dist = dist / 2

    close_enough = False
    if our_point_dist < 2:
        close_enough = True
        our_point_dist = max(dist - 2, 0)

    print(dist, our_point_dist)


    
    goal_pose = Pose()
    goal_pose.position.x = math.cos(angle) * our_point_dist + rover_pose.pose.position.x
    goal_pose.position.y = math.sin(angle) * our_point_dist + rover_pose.pose.position.y


    goal = MoveBaseGoal()
    goal.target_pose.pose = goal_pose
    traverse_action.send_goal(goal)
    res = traverse_action.wait_for_result()
    if res and not close_enough:
        return CONTINUING, None
    elif res:
        return FINISHED, None
    else:
        return FAILURE, None

import math
import rospy
import globalvars


POLL_RATE = 5 # hz

PENDING         = 0   # The goal has yet to be processed by the action server
ACTIVE          = 1   # The goal is currently being processed by the action server
PREEMPTED       = 2   # The goal received a cancel request after it started executing
                      #   and has since completed its execution (Terminal State)
SUCCEEDED       = 3   # The goal was achieved successfully by the action server (Terminal State)
ABORTED         = 4   # The goal was aborted during execution by the action server due
                      #    to some failure (Terminal State)
REJECTED        = 5   # The goal was rejected by the action server without being processed,
                      #    because the goal was unattainable or invalid (Terminal State)
PREEMPTING      = 6   # The goal received a cancel request after it started executing
                      #    and has not yet completed execution
RECALLING       = 7   # The goal received a cancel request before it started executing,
                      #    but the action server has not yet confirmed that the goal is canceled
RECALLED        = 8   # The goal received a cancel request before it started executing
                      #    and was successfully cancelled (Terminal State)
LOST            = 9   # An action client can determine that a goal is LOST. This should not be
                      #    sent over the wire by an action server


def degree_sizes(latitude, longitude):
    lat_deg = 111132.92 - 559.82*math.cos(2*latitude) + 1.175*math.cos(4*latitude) - 0.0023*math.cos(6*latitude)
    long_deg = 111412.84*math.cos(longitude) - 93.5*math.cos(3*longitude) + 0.118*math.cos(5*longitude)
    return lat_deg, long_deg


def gps_to_world(origin, pose):
    lat_deg, long_deg = degree_sizes(origin[0], origin[1])
    x = (pose[0] - origin[0])/lat_deg
    y = (pose[1] - origin[1])/long_deg
    return x, y


def sub_request(topic, dtype, max_wait=60*5):
    def received_msg(msg):
        received_msg.msg = msg
    received_msg.msg = None

    r = rospy.Rate(POLL_RATE);
    rospy.Subscriber(topic, dtype, queue_size=10, callback=received_msg)

    for _ in range(POLL_RATE*max_wait):
        # Check callbacks
        if received_msg.msg is not None:
            # Monitor feedback to know if task is completed
            return received_msg.msg
        r.sleep()
    # Waited MAX_WAIT, timeout
    return None

def quaternion_to_euler(orientation):
    x = orientation.x
    y = orientation.y
    z = orientation.z
    w = orientation.w
    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + y * y)
    roll = math.atan2(t0, t1)
    t2 = +2.0 * (w * y - z * x)
    t2 = +1.0 if t2 > +1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    pitch = math.asin(t2)
    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (y * y + z * z)
    yaw = math.atan2(t3, t4)
    return [yaw, pitch, roll]

def euler_to_quaternion(euler):
    # roll, pitch, yaw = euler
    yaw, pitch, roll = euler

    qx = np.sin(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) - np.cos(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)
    qy = np.cos(roll/2) * np.sin(pitch/2) * np.cos(yaw/2) + np.sin(roll/2) * np.cos(pitch/2) * np.sin(yaw/2)
    qz = np.cos(roll/2) * np.cos(pitch/2) * np.sin(yaw/2) - np.sin(roll/2) * np.sin(pitch/2) * np.cos(yaw/2)
    qw = np.cos(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) + np.sin(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)

    return (qx, qy, qz, qw)

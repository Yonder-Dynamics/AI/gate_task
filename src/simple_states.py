def create_print_state(state):
    def print_state():
        print(state)
        return
    return print_state

#!/usr/bin/env python
import rospy

# States
import gate_spinner
import spiral_search
import go_to_gate
# Create state machine

statemachine = {
    spiral_search.FOUND: go_to_gate.go_to_gate,
    #  go_to_gate.FINISHED: gate_spinner.gate_spinner,
    go_to_gate.CONTINUING: go_to_gate.go_to_gate,
    go_to_gate.TIMEOUT_RECIEVING_GATE: spiral_search.spiral_search,
    go_to_gate.LOST_GATE: spiral_search.spiral_search,
    gate_spinner.TIMEOUT_RECIEVING_GATE: spiral_search.spiral_search,
    gate_spinner.LOST_GATE: spiral_search.spiral_search
}

#current_state = go_to_gate.go_to_gate
current_state = spiral_search.spiral_search
info = None

# Init ROS

rospy.init_node("gate_task")

while True:
    print(current_state.__name__)
    signal, info = current_state(info)
    # Default to print state if state is not on machine
    if signal not in statemachine:
        print("ERROR: {}".format(signal))
        break
    else:
        print("Received signal: {}".format(signal))
        current_state = statemachine[signal]
        print("New state: {}".format(current_state.__name__))

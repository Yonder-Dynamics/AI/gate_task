import rospy
import actionlib
import math
import globalvars
from geometry_msgs.msg import PoseArray, PoseStamped
from move_base_msgs.msg import MoveBaseGoal, MoveBaseAction, MoveBaseActionFeedback
from actionlib_msgs.msg import GoalID
import utils


FINISHED = "path_follower.FINISHED"
TIMEOUT_RECEIVING_PATH = "path_follower.TIMEOUT_RECEIVING_PATH"

RATE = 10 # hz
MAX_WAIT = 60 # seconds

def path_follower():
    # Init ROS stuff
    r = rospy.Rate(RATE);

    path = utils.sub_request(globalvars.FRONTEND_PATH, PoseArray, max_wait=1*60)
    if path is None:
        return TIMEOUT_RECEIVING_PATH
    print(path)

    def received_feedback(msg):
        received_feedback.feedback = msg
    received_feedback.feedback = None

    # Init ROS
    traverse_action = actionlib.SimpleActionClient(
        globalvars.MOVE_ACTION, MoveBaseAction)
    # cancel_action = actionlib.SimpleActionClient(
        # globalvars.CANCEL_MOVE_ACTION, GoalID)
    print("i am waitin for that server")
    traverse_action.wait_for_server()
    # cancel_action.wait_for_server()

    # Convert radial coords to xy
    for i, pose in enumerate(path.poses[::15]):
        # Check callbacks

        # Send pose
        p = PoseStamped()
        p.pose = pose

        goal = MoveBaseGoal()
        goal.target_pose.pose = p.pose
        print("breh im sendin that fuckin goal")
        traverse_action.send_goal(goal)

        print("breh i sent that fuckin goal")
        # Search for gate while monitoring action
        traverse_action.wait_for_result()
        print("got a result")
    # Finished path, not found
    return FINISHED

if __name__ == "__main__":
    rospy.init_node("path_follower")
    print(path_follower())

import rospy
import utils
from geometry_msgs.msg import Pose
from sensor_msgs.msg import NavSatFix
#  from <obs_traversal> import <ObsTraversal>


TIMEOUT_RECIEVING_DEST = "goto_gps_wp.TIMEOUT_RECIEVING_DEST"
TIMEOUT_RECIEVING_ORIGIN = "goto_gps_wp.TIMEOUT_RECIEVING_ORIGIN"
REACHED_DESTINATION = "goto_gps_wp.REACHED_DESTINATION"
FAILURE = "goto_gps_wp.FAILURE"

MAX_WAIT = 5*60 # 5 minutes
RATE = 10 # hz


def goto_gps_wp():

    # Retrieve destination
    gps_goal = utils.sub_request("gps_goal", NavSatFix) # TODO
    gps_origin = utils.sub_request("gps_origin", NavSatFix) # TODO
    if gps_goal is None:
        return TIMEOUT_RECIEVING_DEST
    if gps_origin is None:
        return TIMEOUT_RECIEVING_ORIGIN

    # Received goal, convert to world space
    (x, y) = utils.gps_to_world(
        (gps_origin.latitude, gps_origin.longitude),
        (gps_goal.latitude, gps_goal.longitude))
    goal = Pose()
    goal.position.x = x
    goal.position.y = y

    r = rospy.Rate(RATE);

    # Monitor progress to GPS location
    for _ in range(RATE*MAX_WAIT):
        # Check callbacks

        # Now run service to traverse to goal
        #  rospy.wait_for_service('<obs_traversal>') # TODO
        #  obs_traversal = rospy.ServiceProxy('<obs_traversal>', <ObsTraversal>)
        #  if obs_traversal(goal):
        #      return REACHED_DESTINATION
        #  else:
        #      return FAILURE
        # Wait
        r.sleep()
    # Waited MAX_WAIT, timeout
    return TIMEOUT_RECIEVING_DEST

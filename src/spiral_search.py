import rospy
import actionlib
import math
import globalvars
from geometry_msgs.msg import Pose, PoseArray, PoseStamped
from move_base_msgs.msg import MoveBaseGoal, MoveBaseAction, MoveBaseActionFeedback
import utils
import time
import numpy as np


FOUND = "spiral_search.FOUND"
SEARCH_TIMEOUT = "spiral_search.SEARCH_TIMEOUT"
TIMEOUT_RECIEVING_POSE = "spiral_search.TIMEOUT_RECIEVING_POSE"
GATE_NOT_FOUND = "spiral_search.GATE_NOT_FOUND"

RATE = 10 # hz

REVOLUTIONS = 3
MAX_RADIUS = 3
MIN_RADIUS = 1
# Distance between points
RES = 1

MAX_WAIT = 60 # seconds

def spiral_search(info):
    # Init ROS stuff
    rate = rospy.Rate(RATE);

    rover_pose = utils.sub_request(globalvars.ROVER_POSE_TOPIC, PoseStamped, max_wait=1*60)
    if rover_pose is None:
        return TIMEOUT_RECIEVING_POSE, None

    def received_gate(msg):
        received_gate.gate_rel_pos = msg
    received_gate.gate_rel_pos = None

    def received_feedback(msg):
        received_feedback.feedback = msg
    received_feedback.feedback = None

    # Init ROS
    traverse_action = actionlib.SimpleActionClient(
        globalvars.MOVE_ACTION, MoveBaseAction)
    rospy.Subscriber(globalvars.AR_POSE_TOPIC, PoseArray, queue_size=10,
                     callback=received_gate)
    rospy.Subscriber("{}/feedback".format(globalvars.MOVE_ACTION),
                     MoveBaseActionFeedback, queue_size=10,
                     callback=received_feedback)
    pub = rospy.Publisher("/spiral", PoseArray, queue_size=10)
    traverse_action.wait_for_server()

    yaw = 0

    # Generate spiral path
    heading, _, _ = utils.quaternion_to_euler(rover_pose.pose.orientation)

    angle = heading
    theta = []
    radius = []
    for i in range(MIN_RADIUS, MAX_RADIUS):
        r = REVOLUTIONS / float(MAX_RADIUS-MIN_RADIUS)
        radius.extend(np.linspace(i, i+1, num=int(i*2*math.pi/RES*r)))
        theta.extend(np.linspace(angle, angle+2*r*math.pi, num=int(i*2*math.pi/RES*r)))
        angle = angle+2*r*math.pi

    # Convert radial coords to xy
    for i, (r, t) in enumerate(zip(radius, theta)):
        # Send pose
        p = PoseStamped()
        p.pose.position.x = r*math.cos(t) + rover_pose.pose.position.x
        p.pose.position.y = r*math.sin(t) + rover_pose.pose.position.y

        goal = MoveBaseGoal()
        goal.target_pose.pose = p.pose
        res = traverse_action.send_goal(goal)

        for _ in range(RATE*MAX_WAIT):
            rate.sleep()
            if traverse_action.get_state() == utils.SUCCEEDED:
                # Done with goal
                traverse_action.wait_for_result()
                break
            if received_gate.gate_rel_pos is not None and len(received_gate.gate_rel_pos.poses) > 0:
                # Yay, found it
                traverse_action.cancel_goal()
                return FOUND, received_gate.gate_rel_pos
    # Finished path, not found
    return SEARCH_TIMEOUT, None

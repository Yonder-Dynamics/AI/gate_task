import rospy
from geometry_msgs.msg import Pose

FOUND = "gate_creeper.FOUND"
FAILURE = "gate_creeper.FAILURE"

def gate_creeper():
    # Run the exploration service to find the gates
    # request start of exploration
    # checkup on exploration
    # Listen to AR tag detector
    # Send stop msg to exploration if AR tag is found
    # return err if exploration finishes but no AR tag is found
    rospy.wait_for_service('<obs_traversal>')
    obs_traversal = rospy.ServiceProxy('<obs_traversal>', <ObsTraversal>)
    # Travel to a spot near the gates?
    # Spin around gates
    return SPUN

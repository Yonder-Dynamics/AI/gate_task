import matplotlib.pyplot as plt
import numpy as np
import math

REVOLUTIONS = 3
MAX_RADIUS = 10
MIN_RADIUS = 2
# Distance between points
RES = 1

angle = math.pi/3
theta = []
radius = []
for i in range(MIN_RADIUS, MAX_RADIUS):
    r = REVOLUTIONS / float(MAX_RADIUS-MIN_RADIUS)
    radius.extend(np.linspace(i, i+1, num=int(i*2*math.pi/RES*r)))
    theta.extend(np.linspace(angle, angle+2*r*math.pi, num=int(i*2*math.pi/RES*r)))
    angle = angle+2*r*math.pi

x = []
y = []
for i, (r, t) in enumerate(zip(radius, theta)):
    # Send pose
    x.append(r*math.cos(t))
    y.append(r*math.sin(t))

plt.scatter(x, y)
plt.show()

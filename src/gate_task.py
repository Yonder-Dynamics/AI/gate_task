import rospy

# States
import goto_gps_wp
import gate_spinner
import spiral_search

# Create state machine

statemachine = {
    goto_gps_wp.REACHED_DESTINATION: spiral_search.spiral_search,
    spiral_search.FOUND: gate_spinner.gate_spinner,
}

current_state = goto_gps_wp.goto_gps_wp

# Init ROS

rospy.init_node("gate_task")

while True:
    signal = current_state()
    # Default to print state if state is not on machine
    if signal not in statemachine:
        print(f"ERROR: {signal}")
        break
    else:
        print(f"Received signal: {signal}")
        current_state = statemachine[signal]
        print(f"New state: {current_state.__name__}")
